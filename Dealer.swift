//Dealer.swift
//Blackjack
//
//Author: Charlie Christakos, Team Productivity

import Foundation

class Dealer {

    /////Instance Properties
    var cardNames: [String] = ["heart2", "heart3", "heart4", "heart5", "heart6", "heart7", "heart8", "heart9", "heart10",
    "heartJ", "heartQ", "heartK", "heartA", "spade2", "spade3", "spade4", "spade5", "spade6", "spade7", "spade8", "spade9",
    "spade10", "spadeJ", "spadeQ", "spadeK", "spadeA", "club2", "club3", "club4", "club5", "club6", "club7", "club8", "club9",
    "club10", "clubJ", "clubQ", "clubK", "clubA", "diamond2", "diamond3", "diamond4", "diamond5", "diamond6", "diamond7",
    "diamond8", "diamond9", "diamond10", "diamondJ", "diamondQ", "diamondK", "diamondA"]

    var cardVals: [Int] = [2, 3, 4, 5, 6 ,7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6 ,7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5,
    6 ,7, 8, 9, 10, 10, 10, 10, 11, 2, 3, 4, 5, 6 ,7, 8, 9, 10, 10, 10, 10, 11]

    let player1 =  Player()
    let player2 =  Player()
    let player3 =  Player()

    var winner: Bool = false

    public init()
    {

    }
    func shuffleCards() {
        let newIndicies = cardNames.indices.shuffled()
        cardNames = newIndicies.map { cardNames[$0] }
        cardVals = newIndicies.map { cardVals[$0] }
    }

    func deal(player: Player) {
        if(player.hasBusted() == false) {
            let dealtName = cardNames.remove(at: 0)
            var dealtVal = cardVals.remove(at: 0)
            if(dealtVal == 11 && player.getScore() > 10) {dealtVal = 1}
            player.deckNames.append(dealtName)
            player.deckVals.append(dealtVal)
            player.score += dealtVal
            if(player.getScore() > 21) {player.bust = true}
        }
    }

    func giveEachTwo() {
        deal(player: player1)
        deal(player: player1)
        deal(player: player2)
        deal(player: player2)
        deal(player: player3)
        deal(player: player3)
    }

    func isWinner() {
        if(player1.getScore() == 21) {
            winner = true
            print("The winner is Player 1!")
        }
        else if (player2.hasBusted() && player3.hasBusted()) {
            winner = true
            print("The winner is Player 1!")
        }
        else if (player2.getScore() == 21) {
            winner = true
            print("The winner is Player 2!")
        }
        else if (player1.hasBusted() && player3.hasBusted()) {
            winner = true
            print("The winner is Player 2!")
        }
        else if (player3.getScore() == 21) {
            winner = true
            print("The winner is Player 3!")
        }
        else if (player1.hasBusted() && player2.hasBusted()) {
            winner = true
            print("The winner is Player 3!")
        }
    }

    func printScores()
    {
        print("Player 1: \(player1.score)")
        print("Player 2 : \(player2.score)")
        print("Player 3 : \(player3.score)")
    }

    
}

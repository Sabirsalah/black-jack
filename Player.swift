
//  Player.swift

//  BlackJack Game

//

//  Created by Maslah Ali on 3/30/21.

//


import Foundation

class Player{

    var score: Int = 0
    var deckNames: [String ] = []
    var deckVals: [Int] = []
    var bust: Bool = false
    
   
    // getScore function which goes through decknames, converts them all and takes the sum.

    //Does that mean Ai keeps hitting until it either gets to 21 or busts or will it stop at a certain point.

    // this function would just look at what you have got and count it up.

    func getScore()-> Int{
    //Assume Ace's will have a value 1.
     var total = deckVals.reduce(0,+)
     if total < 12 && deckVals.contains(1){
     
     total = total + 10
     }
     
        return total
    

        //Ace has a value of one, and when you are calculating the score if the Aces in your hand and your score is eleven or less , then add ten to your score.

        // call your convert function, convert all of these decknames into values and then get the sum of the deck values.

        //(Ace's) And then if there is one deck value and the sum you get is less than 12 add 10  to the sum you get.

        

    }

 
    //is it bigger than 21 , well then you busted otherwise you haven't.
    func hasBusted()->Bool{
    
        if getScore() > 21
        {
            bust = true; 
        }
        return bust
    }

    

    //this function would look at what you have got in deck and prompt you to hit or miss.

    func play()->Bool {
    
    if getScore() > 18{
    return false
    }
    else{
    return true
    }

        //it is gone, return hit or pass, depending on.

        //return true or false... return true if you want hit or false if you want to pass.

        

    }

    

    

}


import Foundation


//the main of the programm
@main
class blackJack
{
     static func main() 
     {

          //dealer object 
          let dealer = Dealer()

          // pre requisite to the game starting
          dealer.shuffleCards()
          dealer.giveEachTwo()

          // the while loop that runs the game
          // it continues until there's a winner
          while(dealer.winner == false)
          {
               // scores are printed and player one goes
               print("")
               dealer.printScores()
               if dealer.winner == false && dealer.player1.bust == false
               {
                    print("do you want to be dealt player 1 Y or N")
                    guard let inputOne = readLine() else{return}
                    if(inputOne == "Y" && dealer.winner == false)
                    {
                         dealer.deal(player : dealer.player1)
                         dealer.isWinner()
                    }

                    dealer.printScores()
               }

               // scores are printed again and player 2 goes
               if dealer.winner == false && dealer.player2.bust == false
               {
                    print("do you want to be dealt player 2 Y or N")
                    guard let inputTwo = readLine() else{return}
                    if(inputTwo == "Y" && dealer.winner == false)
                    {
                         dealer.deal(player : dealer.player2)
                         dealer.isWinner()
                    }

                    dealer.printScores()
               }

               // scores are printed and pleayer 3 goes
               if dealer.winner == false && dealer.player3.bust == false
               {
                    print("do you want to be dealt player 3 Y or N")
                    guard let inputThree = readLine() else{return}
                    if(inputThree == "Y" && dealer.winner == false)
                    {
                         dealer.deal(player : dealer.player3)
                         dealer.isWinner()
                    }

                    dealer.printScores()
               }
          }
     }
}